package com.example.potingchiang.nsdchat;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import android.os.Handler;

/**
 * This is for nsd practice
 * Created by potingchiang on 2016-06-10.
 */
public class ChatConnection {

    //declaration
    //classes
    private Handler mUpdateHandler;
    private ChatServer mChatServer;
    private ChatClient mChatClient;
    //variables
    private static final String TAG = "ChatConnection";
    //connection
    private Socket mSocket;
    private int mPort = -1;

    //constructor
    public ChatConnection(Handler handler) {

        this.mUpdateHandler = handler;
        mChatServer = new ChatServer(handler);
    }

    //chat server & client
    //server
    private class ChatServer {

        //declaration
        //variables
        ServerSocket mServerSocket = null;
        Thread mThread = null;

        //constructor
        public ChatServer(Handler handler) {

            mThread = new Thread(new ServerThread());
            mThread.start();
        }
        public void tearDown() {

            mThread.interrupt();
            try {

                mServerSocket.close();

            } catch (IOException e) {

                //error message
//                e.printStackTrace();
                Log.e(TAG, "Error when closing server socket");
            }
        }

        //inner inner class
        private class ServerThread implements Runnable {

            @Override
            public void run() {

                try {

                    // Since discovery will happen via Nsd, we don't need to care which port is
                    // used.  Just grab an available one  and advertise it via Nsd.
                    mServerSocket = new ServerSocket(0);
                    setLocalPort(mServerSocket.getLocalPort());

                    //setup situation
                    while ( ! Thread.currentThread().isInterrupted() ) {

                        Log.d(TAG, "ServerSocket Created, awaiting connection");
                        setSocket(mServerSocket.accept());
                        Log.d(TAG, "Connected");

                        if (mChatClient == null) {

                            int port = mSocket.getPort();
                            InetAddress address = mSocket.getInetAddress();
                            connectToServer(address, port);
                        }
                    }
                }
                catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    //client
    private class ChatClient {

        //declaration
        //variables
        private InetAddress mAddress;
        private int PORT;
        private final String CLIENT_TAG = "ChatClient";
        //thread
        private Thread mSendThread;
        private Thread mRecThread;
        //constructor
        public ChatClient(InetAddress address, int port) {

            //message
            Log.d(CLIENT_TAG, "Creating chatClient");
            //setup variables
            this.mAddress = address;
            this.PORT = port;
            //init Thread
            mSendThread = new Thread(new SendingThread());
            mSendThread.start();
        }

        //inner class sending thread
        private class SendingThread implements Runnable {

            //declaration
            //variables
            BlockingQueue<String> mMessageQueue;
            private int QUEUE_CAPACITY = 10;

            //constructor
            public SendingThread() {

                mMessageQueue = new ArrayBlockingQueue<>(QUEUE_CAPACITY);
            }

            @Override
            public void run() {

                try {

                    if (getSocket() == null) {

                        setSocket(new Socket(mAddress, PORT));
                        Log.d(CLIENT_TAG, "Client-side socket initialized");
                    }
                    else {

                        Log.d(CLIENT_TAG, "Socket already initialized, skipping!");
                    }

                    mRecThread = new Thread(new ReceivingThread());
                    mRecThread.start();

                } catch (UnknownHostException e) {

                    //error message
                    Log.d(CLIENT_TAG, "Inititalizing socket failed, UHE");

                } catch (IOException e) {

                    //error message
                    Log.d(CLIENT_TAG, "Initializing socket failed, IOE");
//                    e.printStackTrace();
                }

                while (true) {

                    try {

                        String msg = mMessageQueue.take();
                        sendMessage(msg);
                    } catch (InterruptedException e) {

                        //error message
                        Log.d(CLIENT_TAG, "Message sending loop interrupted, exiting");
//                        e.printStackTrace();
                    }
                }
            }
        }

        private class ReceivingThread implements Runnable {

            @Override
            public void run() {

                //declaration
                //variables
                BufferedReader input;

                try {

                    input = new BufferedReader(
                            new InputStreamReader(
                                    mSocket.getInputStream()
                            )
                    );
                    while ( ! Thread.currentThread().isInterrupted()) {

                        String messageStr = null;
                        messageStr = input.readLine();
                        if (messageStr != null) {
                            Log.d(CLIENT_TAG, "Read from the stream: " + messageStr);
                            updateMessages(messageStr, false);
                        }
                        else {

                            Log.d(CLIENT_TAG, "The nulls! The nulls!");
                            break;
                        }
                    }

                    input.close();

                } catch (IOException e) {

                    //error message
                    Log.d(CLIENT_TAG, "Server loop error: ", e);
//                    e.printStackTrace();
                }
            }
        }

        //other methods
        //tear down
        public void tearDown() {

            try {

                getSocket().close();
            } catch (IOException e) {

                //error message
                Log.d(CLIENT_TAG, "Error when closing server socket");
//                e.printStackTrace();
            }
        }

        //send message
        public void sendMessage(String msg) {

            try {

                Socket socket = getSocket();

                if (socket == null) {

                    Log.d(CLIENT_TAG, "Socket is null");
                }
                else if (socket.getOutputStream() == null) {

                    Log.d(CLIENT_TAG, "Socket output stream is null");
                }

                //print writer
                PrintWriter out = new PrintWriter(
                        new BufferedWriter(
                                new OutputStreamWriter(getSocket().getOutputStream())), true);
                //out print in line
                out.println(msg);
                out.flush();
                //update message
                updateMessages(msg, true);

            } catch (UnknownHostException e) {

                Log.d(CLIENT_TAG, "Unknown Host", e);

            } catch (IOException e) {
//                e.printStackTrace();
                Log.d(CLIENT_TAG, "I/O Exception", e);
            } catch (Exception e) {
                Log.d(CLIENT_TAG, "Error3", e);
            }

            Log.d(CLIENT_TAG, "Client sent message: " + msg);
        }
    }
    //getter & setter
    //port
    public int getLocalPort() {
        return mPort;
    }
    public void setLocalPort(int port) {
        this.mPort = port;
    }
    //socket
    private synchronized void setSocket(Socket socket) {

        //message
        Log.d(TAG, "setSocket being called");

        //setup socket
        if (socket == null) {

            Log.d(TAG, "Setting a null socket");
        }
        if (mSocket != null) {

            if (mSocket.isConnected()) {

                try {

                    mSocket.close();

                } catch (IOException e) {

                    //error
                    e.printStackTrace();
                }
            }
        }
        mSocket = socket;
    }
    private Socket getSocket() {

        return mSocket;
    }
    //other methods
    // tear down
    public void tearDown() {

        mChatServer.tearDown();
        mChatClient.tearDown();
    }
    //connect to server
    public void connectToServer(InetAddress address, int port) {

        mChatClient = new ChatClient(address, port);
    }
    //send message
    public void sendMessage(String msg) {

        if (mChatClient != null) {

            mChatClient.sendMessage(msg);
        }
    }
    //update
    private synchronized void updateMessages(String msg, boolean local) {

        Log.e(TAG, "Updating message: " + msg);

        if (local) {

            msg = "me: " + msg;
        }
        else {

            msg = "others: " + msg;
        }

        //init bundle
        Bundle messageBundle = new Bundle();
        messageBundle.putString("msg", msg);

        //init message
        Message message = new Message();
        message.setData(messageBundle);
        mUpdateHandler.sendMessage(message);

    }
}
