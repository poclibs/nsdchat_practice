package com.example.potingchiang.nsdchat;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdServiceInfo;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.net.ServerSocket;

/**
 * This class is for NSD(Network Service Discovery)
 * Created by potingchiang on 2016-06-09.
 */
public class NsdHelper {

    //init. variables
    //basic
    private Context mContext;
    //nsd manager
    private NsdManager mNsdManager;
    //register service
    public static final String SERVICE_TYPE_httpTcp = "_http._tcp.";
    public static final String TAG = "NsdHelper";
    //    public String mServiceName = "Po's NSD";
    private String mServiceName = "Po's NSD";
    //nsd service info
    private NsdServiceInfo mServiceInfo;

    //server socket
    private ServerSocket mServerSocket;
    private int mLocalPort;

    //listener
    //registration
    private NsdManager.RegistrationListener mRegistrationListener;
    //discovery
    private NsdManager.DiscoveryListener mDiscoveryListener;
    //resolve
    private NsdManager.ResolveListener mResolveListener;

    //constructor
    public NsdHelper(Context context) {

        this.mContext = context;

        //init nsd manager
        mNsdManager = (NsdManager) context.getSystemService(Context.NSD_SERVICE);
    }

    //init. services
    public void initNsd() {

        initRegistrationListener();
        initDiscoveryListener();
        initResolveListener();
    }
    //register service/optional -> for user can see ur device with info on the local internet
    public void registerService(int port) {

        //create/init. NsdServiceInfo
        NsdServiceInfo serviceInfo = new NsdServiceInfo();

        //setup attributes
        //name might be different based on conflicts
        serviceInfo.setServiceName(mServiceName);
        //setup protocol and transport_layer (Syntax: _protocol._transportLayer.)
        serviceInfo.setServiceType(SERVICE_TYPE_httpTcp);
        //setup port
        serviceInfo.setPort(port);

        //setup registerService
        mNsdManager.registerService(
                serviceInfo,
                NsdManager.PROTOCOL_DNS_SD,
                mRegistrationListener
        );
    }
    //init socket if using it
    public void initServiceSocket() throws IOException{

        // Initialize a server socket on the next available port.
        mServerSocket = new ServerSocket(0);

        // Store the chosen port.
        mLocalPort =  mServerSocket.getLocalPort();
    }
    //init. discoveryServices
    public void discoveryServices() {

        //setup discovery service
        mNsdManager.discoverServices(
                SERVICE_TYPE_httpTcp,
                mNsdManager.PROTOCOL_DNS_SD,
                mDiscoveryListener
        );
    }

    //init listener
    //init registration listener
    public void initRegistrationListener() {

        //init registration listener
        mRegistrationListener = new NsdManager.RegistrationListener() {

            @Override
            public void onRegistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {

                // Registration failed!  Put debugging code here to determine why.

                //message
                Toast.makeText(mContext, "Register failed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUnregistrationFailed(NsdServiceInfo serviceInfo, int errorCode) {

                // Unregistration failed.  Put debugging code here to determine why.

                //message
                Toast.makeText(mContext, "Un-register failed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServiceRegistered(NsdServiceInfo serviceInfo) {

                //update service name
                mServiceName = serviceInfo.getServiceName();

                //message
                Toast.makeText(mContext, "Register successfully!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServiceUnregistered(NsdServiceInfo serviceInfo) {

                // Service has been unregistered.  This only happens when you call
                // NsdManager.unregisterService() and pass in this listener.

                //message
                Toast.makeText(mContext, "Un-register successfully!", Toast.LENGTH_SHORT).show();
            }
        };
    }
    //init discovery service listener
    public void initDiscoveryListener() {

        //init listener
        mDiscoveryListener = new NsdManager.DiscoveryListener() {

            @Override
            public void onStartDiscoveryFailed(String serviceType, int errorCode) {

                //message
                Log.e(TAG, "Discovery failed - Error code: " + errorCode);
                Toast.makeText(mContext, "Discovery failed - Error code: " + errorCode, Toast.LENGTH_SHORT).show();
                //stop service discovery
                mNsdManager.stopServiceDiscovery(this);
            }

            @Override
            public void onStopDiscoveryFailed(String serviceType, int errorCode) {

                //message
                Log.e(TAG, "Discovery failed - Error code: " + errorCode);
                Toast.makeText(mContext, "Discovery failed - Error code: " + errorCode, Toast.LENGTH_SHORT).show();
                //stop service discovery
                mNsdManager.stopServiceDiscovery(this);
            }

            // called as soon as service discovery begins
            @Override
            public void onDiscoveryStarted(String serviceType) {

                //message
                Log.d(TAG, "Service discovery started");
                Toast.makeText(mContext, "Service discovery started.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onDiscoveryStopped(String serviceType) {

                //message
                Log.i(TAG, "Service discovery stopped: " + serviceType);
                Toast.makeText(mContext, "Service discovery stopped: " + serviceType, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServiceFound(NsdServiceInfo serviceInfo) {

                //message
                Log.d(TAG, "Service discovery success" + serviceInfo);
                Toast.makeText(mContext, "Service discovery success" + serviceInfo, Toast.LENGTH_SHORT).show();

                //service found and do with it
                if ( !serviceInfo.getServiceType().equals(SERVICE_TYPE_httpTcp)) {

                    //message
                    //message
                    Log.d(TAG, "Unknown Service Type: " + serviceInfo.getServiceType());
                    Toast.makeText(mContext, "Unknown Service Type: " + serviceInfo.getServiceType(),
                            Toast.LENGTH_SHORT).show();

                }
                else if (serviceInfo.getServiceName().equals(mServiceName)) {

                    //message
                    Log.d(TAG, "Same machine: " + mServiceName);
                    Toast.makeText(mContext, "Same machine: " + mServiceName, Toast.LENGTH_SHORT).show();
                }
                else if (serviceInfo.getServiceName().contains(mServiceName)) {

                    //init resolve listener
                    mNsdManager.resolveService(serviceInfo,mResolveListener);
                }
//                else {
//
//                    //init resolve listener
//                    mNsdManager.resolveService(serviceInfo,mResolveListener);
//                }
            }

            @Override
            public void onServiceLost(NsdServiceInfo serviceInfo) {

                // When the network service is no longer available.
                // Internal bookkeeping code goes here.
                //message
                Log.e(TAG, "Service lost" + serviceInfo);
                Toast.makeText(mContext, "Service lost" + serviceInfo, Toast.LENGTH_SHORT).show();
                //setup mService if its null
                if (mServiceInfo == serviceInfo) {

                    mServiceInfo = null;
                }

            }
        };
    }
    //init resolve listener
    public void initResolveListener() {

        //init listener
        mResolveListener = new NsdManager.ResolveListener() {

            @Override
            public void onResolveFailed(NsdServiceInfo serviceInfo, int errorCode) {

                //message
                Log.e(TAG, "Resolve failed" + errorCode);
                Toast.makeText(mContext, "Resolve failed" + errorCode, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onServiceResolved(NsdServiceInfo serviceInfo) {

                //message
                Log.e(TAG, "Resolve succeeded" + serviceInfo);
                Toast.makeText(mContext, "Resolve succeeded" + serviceInfo, Toast.LENGTH_SHORT).show();
                //check if its the same
                if (serviceInfo.getServiceName().equals(mServiceName)) {

                    Log.d(TAG, "Same name/ip");

                    return;
                }
                mServiceInfo = serviceInfo;
            }
        };
    }

    //teardown/stop service
    public void tearDown() {

        //un-register service
        mNsdManager.unregisterService(mRegistrationListener);
        //stop discovery
        mNsdManager.stopServiceDiscovery(mDiscoveryListener);
    }

    //getter & setter
    public NsdServiceInfo getChosenServiceInfo() {

        return mServiceInfo;
    }

}
