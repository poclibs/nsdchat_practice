package com.example.potingchiang.nsdchat;

import android.content.Intent;
import android.net.nsd.NsdServiceInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    //variables
    //layout elements
    private TextView mstatusView;
    //tag
    public static final String TAG = "NsdChat";
    //nsd helper
    private NsdHelper nsdHelper;
    //handler
    private Handler mUpdateHandler;
    //connection
    private ChatConnection mConnection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //init layout elements
        mstatusView = (TextView) findViewById(R.id.status);

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //init message update handler
        mUpdateHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String chatLine = msg.getData().getString("msg");
                addChatLine(chatLine);
            }
        };

        //init connection
        mConnection = new ChatConnection(mUpdateHandler);

        //init nsd helper
        nsdHelper = new NsdHelper(MainActivity.this);
        //init nsd service
        nsdHelper.initNsd();
    }

    //on ac lifecycle
    @Override
    protected void onPause() {

        //parent onPause
        super.onPause();
        //tear down service
        if (nsdHelper != null) {
            nsdHelper.tearDown();
        }
    }
    @Override
    protected void onResume() {

        //parent onResume
        super.onResume();
        //re-start service
        if (nsdHelper != null) {

            nsdHelper.registerService(mConnection.getLocalPort());
            nsdHelper.discoveryServices();
        }
    }
    @Override
    protected void onDestroy() {

        //parent onDestroy
        super.onDestroy();
        //tear service
        nsdHelper.tearDown();
        //tear down connection as well
        mConnection.tearDown();
    }

    //on click listener
    //registration
    public void clickAdvertise(View v) {

        //register service
        if (mConnection.getLocalPort() > -1) {

            nsdHelper.registerService(mConnection.getLocalPort());
        }
        else {

            Log.d(TAG, "ServerSocket isn't bound.");
        }
    }
    //discovery
    public void clickDiscover(View v) {

        nsdHelper.discoveryServices();
    }
    //connection
    public void clickConnect(View v) {

        //get chosen service
        NsdServiceInfo service = nsdHelper.getChosenServiceInfo();
        if (service != null) {

            Log.d(TAG, "Connecting");
            mConnection.connectToServer(
                    service.getHost(),
                    service.getPort()
            );
        }
        else {

            Log.d(TAG, "No service tp connect to!");
        }
    }
    //send
    public void clickSend(View v) {

        //init edit text
        EditText messageView = (EditText) findViewById(R.id.chatInput);
        //get message from layout
        //check if its null
        if (messageView != null) {

            //get messages
            String messageString = messageView.getText().toString();
            //check if its empty
            if ( ! messageString.isEmpty()) {

                //send message if its not empty
                mConnection.sendMessage(messageString);
            }

            //clear/empty messages from layout
            messageView.setText("");
        }
    }

    //other methods
    //add message
    public void addChatLine(String line) {

        mstatusView.append("\n" + line);
    }

}
